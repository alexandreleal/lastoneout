﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour {


    public enum GameState
    {
    Stoped = 0,
    Running = 1,
    Paused = 2,
    GameOver = 3
    };

    public enum Player
    {
        Local = 0,
        Other = 1
    }


    #region Variables

    #region AR_Vars
    private Gyroscope gyro;
    private GameObject cameraContainer;
    private Quaternion rotation;

    [SerializeField]
    private Transform objCam;
    private WebCamTexture cam;
    [SerializeField]
    private RawImage background;
    [SerializeField]
    private AspectRatioFitter fit;
    [SerializeField]
    private GameObject boardGame;

    private bool arReady = false;
    #endregion

    /// <summary>
    /// The number of max moves player can do
    /// </summary>
    private const int MAX_NUMBER_OF_MOVES = 3;

    /// <summary>
    ///  State of the game
    /// </summary>
    public GameState gameState = GameState.Stoped;

    /// <summary>
    /// Used to know which player is playing
    /// </summary>
    public Player player = Player.Local;

    /// <summary>
    /// Count the number of moves of actual player
    /// </summary>
    [SerializeField]
    private int numberOfMoves;

    /// <summary>
    /// block of player infos
    /// </summary>
    [SerializeField]
    private GameObject pnlInfos;

    /// <summary>
    /// Show how is playing this turn
    /// </summary>
    [SerializeField]
    private Text txtPlayerTurn;

    /// <summary>
    ///  Victory texto to show on screen
    /// </summary>
    [SerializeField]
    private GameObject txtResult;

    /// <summary>
    ///  Show how many movements the actual player still have
    /// </summary>
    [SerializeField]
    private Text txtPlayerMoves;

    /// <summary>
    /// Button that represent the pieces of the game
    /// </summary>
    [SerializeField]
    private Piece prefabButtonPiece;

    /// <summary>
    /// Number of max pieces in the game
    /// </summary>
    private int numberOfPieces = 20;

    /// <summary>
    /// List of pieces on the game
    /// </summary>
    [SerializeField]
    private List<Piece> arrPiecesOnGame = new List<Piece>();

    /// <summary>
    /// Represent the parent of the pieces
    /// </summary>
    [SerializeField]
    private Transform board;

    /// <summary>
    /// Start the game when pressed
    /// </summary>
    [SerializeField]
    private Button btPlay;

    /// <summary>
    /// Unpause de game or restart it when the game finish
    /// </summary>
    [SerializeField]
    private Button btContinue;

    /// <summary>
    /// Finish the game and go back to menu
    /// </summary>
    [SerializeField]
    private Button btGiveUp;

    /// <summary>
    /// Panel that goes up the game to block all the clicks
    /// </summary>
    [SerializeField]
    private GameObject blockPanel;

    /// <summary>
    /// Block local player from play on others turn
    /// </summary>
    [SerializeField]
    private GameObject blockPlayer;

    [SerializeField]
    private GameObject btPassTurn;

    #endregion

    /// <summary>
    /// Start the script
    /// </summary>
    private void Start () {

        // prepare system for AR
        arReady = PrepareAR();

        // on btPlay click create the board on the screen
        btPlay.onClick.AddListener(delegate { ManageGameState(); });
        btContinue.onClick.AddListener(delegate { ManageGameState(); });
        btGiveUp.onClick.AddListener(delegate { StartCoroutine(GameOver()); });

        // set button to right position
        btPlay.GetComponentInParent<RectTransform>().sizeDelta = new Vector2(450, 200);
        btPlay.GetComponentInParent<RectTransform>().localPosition = new Vector2(0, 410);

        // create a pool for object pieces
        prefabButtonPiece.CreatePool(numberOfPieces);
    }

    private void Update()
    {
        if(arReady)
        {
            float ratio = (float)cam.width / (float)cam.height;
            fit.aspectRatio = ratio;

            float scaleY = cam.videoVerticallyMirrored ? -1.0f : 1.0f;
            background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

            int orientation = -cam.videoRotationAngle;
            background.rectTransform.localEulerAngles = new Vector3(0, 0, orientation);

            objCam.localRotation = gyro.attitude * rotation;
        }
    }

    private bool PrepareAR()
    {
        try
        {
            // set the camera to be the back camera
            Debug.Log(WebCamTexture.devices.Length);
            for (int i = 0; i < WebCamTexture.devices.Length; i++)
            {
                Debug.Log(WebCamTexture.devices[i].name + ": front facing = " + WebCamTexture.devices[i].isFrontFacing);
                if (!WebCamTexture.devices[i].isFrontFacing)
                {
                    cam = new WebCamTexture(WebCamTexture.devices[i].name, Screen.width, Screen.height);
                }
            }

            cameraContainer = new GameObject("Camera Container");
            cameraContainer.transform.position = objCam.position;
            objCam.SetParent(cameraContainer.transform);

            gyro = Input.gyro;
            gyro.enabled = true;
            cameraContainer.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            rotation = new Quaternion(0, 0, 1, 0);

            cam.Play();
            background.texture = cam;
            return true;
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Create the board and set pieces to their places
    /// </summary>
    /// <returns></returns>
    private void StartGame()
    {
        // define games variables
        numberOfMoves = MAX_NUMBER_OF_MOVES;
        player = Player.Local;
        blockPanel.SetActive(false);
        blockPlayer.SetActive(player == Player.Local ? false : true);
        btPlay.GetComponentInParent<RectTransform>().sizeDelta = new Vector2(200, 200);
        btPlay.GetComponentInParent<RectTransform>().localPosition = new Vector2(440, 860);
        btPlay.GetComponentInChildren<Text>().text = "||";
        pnlInfos.SetActive(true);
        txtPlayerTurn.text = player == Player.Local ? "Your Turn" : "Computer Turn";
        txtPlayerMoves.text = "Moves Left: " + numberOfMoves.ToString();

        boardGame.transform.localRotation = objCam.localRotation;

        int index = 1;
        // create a cartesian for pieces
        for (int y = 0; y < 5; y++)
        {
            for (int x = 0; x < 5; x++)
            {
                if(!((x == 0 && y == 0) || (x == 4 && y == 0) || (x == 2 && y == 2) || (x == 0 && y == 4) || (x == 4 && y == 4)))
                {
                    // spawn piece and set their infos
                    //Piece temp = prefabButtonPiece.Spawn(board, new Vector3(-400 + x * 200, 400 - y * 200, 0));
                    Piece temp = prefabButtonPiece.Spawn(board, new Vector3(-0.0965f + x * 0.04825f, 0, 0.0965f - y * 0.04825f));
                    temp.GetComponentInParent<Transform>().localScale = new Vector3(0.2f, 0.2f, 0.2f);
                    temp.GetComponentInParent<Transform>().localEulerAngles = new Vector3(90, 0, 0);
                    temp._main = this;
                    temp.name = index.ToString();
                    temp.Index = index++;
                    arrPiecesOnGame.Add(temp);
                }
            }
        }
    }

    /// <summary>
    /// Manage the actual state of the game
    /// </summary>
    private void ManageGameState()
    {
        if (gameState == GameState.Stoped)
        {
            gameState = GameState.Running;
            StartGame();
        }
        else if (gameState == GameState.Running)
        {
            gameState = GameState.Paused;
            StartCoroutine(PauseManager());
        }
        else if (gameState == GameState.Paused)
        {
            gameState = GameState.Running;
            boardGame.transform.localRotation = objCam.localRotation;
            StartCoroutine(PauseManager());
        }
        else
        {
            StartCoroutine(GameOver());
        }
    }

    /// <summary>
    /// Manage the player turn
    /// </summary>
    private void ManageTurn(bool passTurn = false)
    {
        // verify which player has to play next
        if(numberOfMoves == 0 || passTurn)
        {
            /// remove button pass turn from screen
            btPassTurn.SetActive(false);

            // block local player and start computer AI
            if (player == Player.Local)
            {
                numberOfMoves = MAX_NUMBER_OF_MOVES;
                player = Player.Other;
                pnlInfos.GetComponent<Image>().color = Color.red;
                blockPlayer.SetActive(true);
                StartCoroutine(AI());
            }
            // free local player again
            else
            {
                numberOfMoves = MAX_NUMBER_OF_MOVES;
                player = Player.Local;
                pnlInfos.GetComponent<Image>().color = Color.green;
                blockPlayer.SetActive(false);
            }

            // update player turn on screen
            txtPlayerTurn.text = player == Player.Local ? "Your Turn" : "Computer Turn";
            // update number of moves left
            txtPlayerMoves.text = "Moves Left: " + numberOfMoves.ToString();


        }
    }

    /// <summary>
    /// Manage the turn when player click on button pass turn
    /// </summary>
    /// <param name="value"></param>
    public void PassTurn(bool value)
    {
        ManageTurn(value);
    }

    /// <summary>
    /// Block all game changes but the buttons Unpause/Restart/Leave Game
    /// </summary>
    /// <returns></returns>
    private IEnumerator PauseManager()
    {
        blockPanel.SetActive(true);
        txtResult.GetComponentInChildren<Text>().text = "Game Paused";
        btContinue.GetComponentInChildren<Text>().text = "Resume";
        btGiveUp.GetComponentInChildren<Text>().text = "Give Up";

        while(gameState == GameState.Paused)
        {
            yield return new WaitForEndOfFrame();
        }
        blockPanel.SetActive(false);
    }

    /// <summary>
    /// Clear the board, show the results and go to main screen
    /// </summary>
    /// <returns></returns>
    private IEnumerator GameOver()
    {
        blockPanel.SetActive(true);
        btPassTurn.SetActive(false);
        btContinue.GetComponentInChildren<Text>().text = "Play Again";
        btGiveUp.GetComponentInChildren<Text>().text = "Menu";

        // recycle all pieces on screen
        for (int i = 0; i < arrPiecesOnGame.Count; i++)
        {
            arrPiecesOnGame[i].Recycle();
        }

        // clear array of pieces
        arrPiecesOnGame.Clear();

        if (gameState == GameState.GameOver)
        {
            StartCoroutine(ShowResults());
        }
        else
        {
            gameState = GameState.Stoped;
            blockPanel.SetActive(false);
            pnlInfos.SetActive(false);
            btPlay.GetComponentInParent<RectTransform>().sizeDelta = new Vector2(450, 200);
            btPlay.GetComponentInParent<RectTransform>().localPosition = new Vector2(0, 410);
            btPlay.GetComponentInChildren<Text>().text = "Play";
            btPassTurn.SetActive(false);

        }

        yield return null;
    }

    /// <summary>
    /// Show result of the game on screen
    /// </summary>
    /// <returns></returns>
    private IEnumerator ShowResults()
    {
        // clear the gamestate variable
        gameState = GameState.Stoped;
        txtResult.GetComponentInChildren<Text>().text = player == Player.Local ? "You Win!!!" : "You Lose";


        yield return null;
    }

    /// <summary>
    /// Used to recycle te pieces and clean the list
    /// </summary>
    /// <param name="value"></param>
    public void RemoveFromBoard(Piece _object)
    {
        // remove piece from list of playable ones
        arrPiecesOnGame.Remove(_object);

        numberOfMoves--;
        
        // update number of moves left
        txtPlayerMoves.text = "Moves Left: " + numberOfMoves.ToString();

        // verify if still have pieces to play
        if (arrPiecesOnGame.Count == 1)
        {
            gameState = GameState.GameOver;
            ManageGameState();
        }
        // show end turn button if local player  
        else if (player == Player.Local && numberOfMoves == 2)
        {
            btPassTurn.SetActive(true);
        }
        // end turn if movements end
        else if(numberOfMoves == 0)
        {
            ManageTurn();
        }
    }

    /// <summary>
    /// Computer player that will chose random pieces in its turn
    /// </summary>
    private IEnumerator AI()
    {
        // select a number of pieces to remove
        System.Random rnd = new System.Random();
        // temp variable to count number of pieces removed
        int count = rnd.Next(1, numberOfMoves + 1);
        bool pass = count < 3 ? true : false;

        Debug.Log(count);

        yield return new WaitForSeconds(1.0f);

        // loop until end moves
        while (count > 0)
        {
            // wait for game running to play
            while (count > 0 && gameState == GameState.Running && player == Player.Other)
            {
                int index = rnd.Next(0, arrPiecesOnGame.Count);
                arrPiecesOnGame[index].RemovePieceFromGame();
                count--;
                yield return new WaitForSeconds(1.0f);
            }

            // stop the coroutine if the game ends
            if(gameState == GameState.GameOver || player == Player.Local)
            {
                yield break;
            }

            yield return new WaitForEndOfFrame();
        }

        // pass the turn cause AI haven't used all movements
        if (pass)
        {
            PassTurn(true);
        }
    }
}