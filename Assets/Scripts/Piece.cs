﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Piece : MonoBehaviour {

    /// <summary>
    /// The place of the piece on the board
    /// </summary>
    private int index;
    public int Index
    {
        get
        {
            return index;
        }
        set
        {
            index = value;
        }
    }
    /// <summary>
    /// Reference of the main script
    /// </summary>
    public Main _main;

    // Use this for initialization
    /*void Start () {
        this.GetComponent<Button>().onClick.AddListener(delegate
        {
            RemovePieceFromGame();
        });
	}*/


    public void OnMouseDown()
    {
        if (_main.gameState == Main.GameState.Running &&
            _main.player == Main.Player.Local)
        {
            RemovePieceFromGame();
        }
    }


    public void RemovePieceFromGame()
    {
        Debug.Log("Teste " + index);
        _main.RemoveFromBoard(this);
        this.Recycle();
    }

    
}
